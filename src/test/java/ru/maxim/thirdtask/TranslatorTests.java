package ru.maxim.thirdtask;

import com.alibaba.fastjson.JSON;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Assert;
import org.junit.Test;
import ru.maxim.thirdtask.domain.YandexAnswer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TranslatorTests {
    private static final String API_KEY =
            "trnsl.1.1.20190320T130646Z.9d3fbab4be10dbd7.2a5f4077b581e550946572798793d84f710ce302";
    private static final String PATH =
            "https://translate.yandex.net/api/v1.5/tr.json/translate";

    @Test
    public void testConnectionAndTranslation() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(PATH);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("key", API_KEY));
        params.add(new BasicNameValuePair("lang", "en-ru"));
        params.add(new BasicNameValuePair("text", "Hello"));

        httpPost.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));
        HttpResponse response = client.execute(httpPost);

        YandexAnswer yandexAnswer = JSON.parseObject(response.getEntity().getContent(), YandexAnswer.class);

        Assert.assertNotNull(yandexAnswer);
        Assert.assertEquals(yandexAnswer.getCode(), "200");
        Assert.assertEquals(yandexAnswer.getText(), "Рус: Привет");
    }
}
