package ru.maxim.thirdtask.domain;

public class YandexAnswer {
    private String code;
    private String lang;
    private String[] text;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getText() {
        return "Рус: " + text[text.length - 1];
    }

    public void setText(String[] text) {
        this.text = text;
    }
}
